//
//  CustomTVC.swift
//  Bills
//
//  Created by Edgar Figueroa on 11/24/16.
//  Copyright © 2016 Edgar Figueroa. All rights reserved.
//

import UIKit

class CustomTVC: UITableViewCell {
    
    @IBOutlet weak var myPhoto  : UIImageView!
    @IBOutlet weak var product  : UILabel!
    @IBOutlet weak var deadLine : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
