//
//  ViewController.swift
//  Bills
//
//  Created by Edgar Figueroa on 11/23/16.
//  Copyright © 2016 Edgar Figueroa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func Debts(_ sender: UIButton) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller : AllDebts = storyboard.instantiateViewController(withIdentifier: "AllDebts") as! AllDebts
        self.present(controller, animated: true, completion: nil)
        
    }
    
    @IBAction func DebtInfo(_ sender: UIButton) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller2: DebtInfo      = storyboard.instantiateViewController(withIdentifier: "DebtInfo") as! DebtInfo
        self.present(controller2, animated: true, completion: nil)

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

