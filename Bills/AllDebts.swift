//
//  CreditCardsController.swift
//  Bills
//
//  Created by Edgar Figueroa on 11/23/16.
//  Copyright © 2016 Edgar Figueroa. All rights reserved.
//

import UIKit

class AllDebts: UIViewController {

    let myArray         : [String] = ["mac","bici"]
    let deadLineArray   : [String] = ["18 mayo","4 de septiembre"]
    
    @IBOutlet weak var myTableView : UITableView!
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.myTableView.delegate   = self
        self.myTableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension AllDebts : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let cell : String? = self.myArray[indexPath.row]
    
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller2: DebtInfo      = storyboard.instantiateViewController(withIdentifier: "DebtInfo") as! DebtInfo
        controller2.theProductName = cell
        self.present(controller2, animated: true, completion: nil)
        
        
    }
    
}

extension AllDebts : UITableViewDataSource {
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.myTableView.dequeueReusableCell(withIdentifier: "CustomTVC") as! CustomTVC
        cell.product.text = self.myArray[indexPath.row]
        cell.deadLine.text = self.deadLineArray[indexPath.row]
        
        return cell
    }
    
}


