//
//  AddBill.swift
//  Bills
//
//  Created by Edgar Figueroa on 11/27/16.
//  Copyright © 2016 Edgar Figueroa. All rights reserved.
//

import UIKit

class DebtInfo: UIViewController {
    
    @IBOutlet weak var productName: UITextField!
    @IBOutlet weak var showDeadLine: UITextField!
    
    var theProductName : String?
    let datePicker : UIDatePicker = UIDatePicker()
    let done : UIButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.productName.text = theProductName
        datePicker.datePickerMode = UIDatePickerMode.date
        done.setTitle("Done", for: UIControlState.normal)
        done.setTitle("Done", for: UIControlState.highlighted)
        done.setTitleColor(UIColor.black, for: UIControlState.normal)
        done.setTitleColor(UIColor.gray, for: UIControlState.highlighted)
        
    }
    @IBAction func datePickerValueSet(_ sender: UITextField) {
        sender.inputView = datePicker
        datePicker.addTarget(self, action: #selector(DebtInfo.datePickerValueChanged), for: UIControlEvents.valueChanged)
        done.addTarget(self, action: #selector(DebtInfo.datePickerEndEditing(sender:)), for: UIControlEvents.editingDidEnd)
        
    }

    func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        showDeadLine.text = dateFormatter.string(from: sender.date)
        
    }
    
    func datePickerEndEditing(sender:UIButton) {
        datePicker.resignFirstResponder()
    }
    
    
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
